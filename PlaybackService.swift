extension PlaybackService {
    func reset() {
        self.playbackPosition = 0.0
    }
    func secondsIntValue() -> Int {
        guard let value = self.playedTime().value else { return 0 }
        let duration = value.int64Value / 1000
        let positiveDuration = llabs(duration)
        let seconds = Int(positiveDuration % 60)
        return seconds
    }
    func shouldReset() -> Bool {
        return self.secondsIntValue() > 0
    }
}
